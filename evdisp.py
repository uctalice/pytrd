#!/usr/bin/env python3

#import defaults
import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import itertools
import argparse
from roi import regions_of_interest



if __name__ == "__main__":

    # ------------------------------------------------------------------------
    # generate a parser for the command line arguments
    parser = argparse.ArgumentParser(description='Generate a pulse-height plot.')
    parser.add_argument('filename', help='the TRD raw data file to process')
    parser.add_argument('--nevents', '-n' , default=1000, type=int,
                        help='number of events to analyse')
    parser.add_argument('--progress', '-p' , default=-1, type=int,
                        help='print event number every N events')
    parser.add_argument('--allplots', action='store_true',
                        help='draw a crowded plot with all tracklets')
    parser.add_argument('--printargs', action='store_true',
                        help='print arguments and exit')

    args = parser.parse_args()

    if args.printargs:
        print (args)
        exit(0)

    # ------------------------------------------------------------------------
    # setup the reader
    reader = rdr.o32reader(args.filename)
    analyser = adc.adcarray()
    # analyser.debug = 8

    # ------------------------------------------------------------------------
    # some default settings
    DATA_EXCLUDE_MASK = np.zeros((12, 144, 30), dtype=bool)
    # DATA_EXCLUDE_MASK[4:8,0:72,:] = True
    DATA_EXCLUDE_MASK[8:12,0:72,:] = True

    sumtrkl = np.zeros(30)
    ntrkl = 0


    # ------------------------------------------------------------------------
    # create the event display frame
    fig,rowplt = plt.subplots(12,1, sharex=True)


    # ------------------------------------------------------------------------
    # event loop
    for evno, raw_data in enumerate(reader):

        # limit number of events to be processed
        if evno >= args.nevents: break

        # skip the first event, which is usually a config event
        if evno == 0: continue

        if args.progress > 0 and evno%args.progress==0:
            print ("###  EVENT %d" % evno )

        # read the data
        try:
            analyser.analyse_event(raw_data)
        except adc.datafmt_error as e:
            print ("data format error in event %d" % evno)
            print(e)
            continue

        data = analyser.data[:12]  # The last four rows are zeros.
        data[DATA_EXCLUDE_MASK] = 0

        # draw the raw data
        for r in range(12):
            rowplt[r].imshow(data[r,:,:].transpose(), aspect='auto')

            for p in rowplt[r].patches:
                p.remove()

        # highlight regions of interest
        for roi in regions_of_interest(data):

            roi['event'] = evno
            print (roi)

            trkl = data[roi['row'], roi['start']:roi['end'], :]-9.5

            # skip roi if data in first bins
            if ( np.sum(trkl[:,0:6]) > 50 ): continue

            rect = patches.Rectangle((roi['start'],0),roi['npad'],29,
                                     linewidth=2,edgecolor='r',
                                     facecolor='none')

            rowplt[roi['row']].add_patch(rect)

            # fill pulseheight sum and plot tracklets
            sumtrkl += np.sum(trkl, 0)
            ntrkl += 1
            #plt.plot(np.sum(trkl,0))


        #plt.show()
        plt.waitforbuttonpress()

    #plt.figure()
    #plt.plot(sumtrkl/ntrkl)
    #plt.show()
