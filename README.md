
PyTRD - Python module and scripts to analyze ALICE TRD data
===========================================================

Contributing
------------

If you are working on the ALICE TRD at UCT, you will probably develop some 
code for your data analysis. Please share this code with others, including 
future generations of students, by contributing your changes to this 
repository. 

Although all contributions are welcome, we will review them before merging 
them. The reason for this is that we want to keep the repository clean and 
easy to access for new users. We might ask you to add some documentation or 
clean up some things that got mangled up in the development process. To make 
this process as easy and painless as possible, it will be handled through 
pull requests. It sounds more complicated than it is - you will just have to 

 - fork this repository: this makes sure that all your changes can be easily 
   integrated into the overall project history
 - commit your changes to your fork (or copy) of the repository
 - create a pull request: this starts a short review of your changes, which 
   will then be merged into the overall project history
   
Background
----------

This repository is to consolidate other repositories where code related to
the ALICE TRD chamber at UCT has been commited. All relevant code should be
collected here, and this repository should be the basis for future work. 

This repository is based on code from the following repositories: 

 - https://bitbucket.org/uctalice/trdraw/ contains code from two third year 
   and honours projects, namely the projects by Raynette and Jan-Albert.

 - https://github.com/jerowilko/ALICETRD2018 s the code from the first time
   the third year prac on the ALICE TRD was run in 2018. This is a good 
   starting point for new developments, but if you find anything interesting 
   in that repository, migrate the code to this repository as explained in 
   the ["Contributing" section](#contributing).



