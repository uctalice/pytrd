#!/usr/bin/env python3

import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib.pyplot as plt
from sys import argv
import argparse


# ---------------------------------------------------------------------------
# Data boundaries

parser = argparse.ArgumentParser(description='Generate a pulse-height plot.')
parser.add_argument('filename', help='the TRD raw data file to process')
parser.add_argument('--nevents', '-n' , default=1000, type=int,
                        help='number of events to analyse')
args = parser.parse_args()

DATA_EXCLUDE_MASK = np.zeros((12, 144, 30), dtype=bool)
DATA_EXCLUDE_MASK[4:8, 0:72, :] = True

reader = rdr.o32reader(args.filename)
analyser = adc.adcarray()

tbsum_mask = 299
tbsum_interest = 450

padrowlist = []
padlist = []


# ---------------------------------------------------------------------------
# Data analysis

for evno, raw_data in enumerate(reader):
    # if evno % defaults.PRINT_EVNO_EVERY == 0:
    #     print("Proccessing events %d–%d"
    #             % (evno, evno + defaults.PRINT_EVNO_EVERY))

    if evno >= args.nevents: break

    if evno == 0:
        print ("Skipping first event - possible config event")
        continue

    if evno != 0:
        print('EVENT', evno)


    try:
        analyser.analyse_event(raw_data)
    except adc.datafmt_error as e:
        continue
    data = analyser.data[:12]  # The last four rows are zeros.
    data[DATA_EXCLUDE_MASK] = 0
    tbsum = np.sum(data, 2)
    # print(tbsum)
    tbsum_zeroes = tbsum < tbsum_mask
    #tbsum[tbsum_zeroes] = 0
    point_of_interest = np.argwhere(tbsum > tbsum_interest)

#    if len(point_of_interest) == 0: continue
    visited = np.zeros([12,144],dtype=bool)
    roimask = np.zeros((12, 144, 30), dtype=bool)

    lmax = 0
    evsp = 0
    evsn = 0
    p = None
    r = None
    for value, loc in enumerate(point_of_interest):
        # print('     ', loc[0], '   ', loc[1], '   ', tbsum[loc[0], loc[1]])
        # print('       Points of interest in event: ', len(point_of_interest))
        # ---------------------------------------------------------------
        # To extend the region of interest
        uphit = loc[1]
        upmrg = loc[1]
        if visited[loc[0],loc[1]]: continue
        visited[loc[0],loc[1]] = True
        for i in range(loc[1]+1, 144):
            if  tbsum[loc[0],i] > 310: uphit = i
            upmrg = i
            visited[loc[0],i] = True
            if upmrg >= uphit+1: break

        lohit = loc[1]
        lomrg = loc[1]

        for i in range(loc[1]-1, -1, -1):
            if  tbsum[loc[0],i] > 310: lohit = i
            lomrg = i
            visited[loc[0],i] = True
            if lomrg <= lohit-1: break

        print (lomrg,lohit,loc,uphit,upmrg)
        trkldata = data[loc[0],lomrg:upmrg+1,:]
        roimask[loc[0],lomrg:upmrg+1,:] = True
        # if np.sum(trkldata) > 4000: continue

        print(trkldata.transpose())
        print(np.sum(trkldata, 1))
        print(np.sum(trkldata))

    # =================================================================
    # Show roimask
    roilist = np.argwhere(np.sum(roimask,axis=2)>0)

    bgpadmask = np.logical_and(
        np.logical_not(roimask.any(axis=2)),
        np.logical_not(DATA_EXCLUDE_MASK.any(axis=2)))
    bglist = np.argwhere(bgpadmask)

    print(len(roilist), len(bglist))

    if len(roilist) == 0:
        print("No ROI in event", evno)
    else:
        print(roilist)
        print(np.argwhere(roimask.any(axis=2)))

    # =================================================================
    # Plotting the data
    #plt.imshow(tbsum, cmap='hot', vmin=tbsum_mask - 1, norm=None, extent=None, aspect=None)
    #plt.colorbar(orientation='horizontal')
    #plt.show()
