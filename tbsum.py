#!/usr/bin/env python3

"""python3 tbsum.py [FILE...]

Produce a histogram of events summed over time bins.

Extract events from raw data files, sum over the time bins, and save a
histogram in a file called tbsum.png.

Parameters
----------
FILE : str
    list of arbitrary length of paths to raw data files, relative to this
    file's directory.  If this parameter is absent, process all files in
    `defaults.DEFAULT_DATA_FOLDER`.
"""

import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import glob
import sys

mpl.rcParams['font.size'] = 12
array_shape = (12, 144, 30)
num = 2000
bins = np.linspace(1, 20000, num)

PRINT_EVNO_EVERY = 100
DEFAULT_DATA_FOLDER = './'
DATA_EXCLUDE_MASK = np.zeros((12, 144, 30), dtype=bool)
DATA_EXCLUDE_MASK [4:8, 0:72, :] = True

def sum_events(path):
    """Extract events from a file, and sum the data arrays.
    
    Parameters
    ----------
    path : str
        Path to a raw data file
    """
    reader = rdr.o32reader(path)
    analyser = adc.adcarray()
    inner_hist, bin_edges = np.histogram((), bins)

    for evno, raw_data in enumerate(reader):
        if evno % PRINT_EVNO_EVERY == 0:
            print("Proccessing events %d–%d"
                    % (evno, evno + PRINT_EVNO_EVERY))

        if evno == 0:
            # Skip the first event as it may be a configuration event
            # depending on run configurations.
            continue 

        if evno == 500:
            break
        
        try:
            analyser.analyse_event(raw_data)
        except adc.datafmt_error as e:
            continue
        data = analyser.data[:12] # The last four rows are zeros.
        data[DATA_EXCLUDE_MASK] = 0.0
        tbsum = np.sum(data, 2)
        
        localmax = np.zeros(np.shape(tbsum), dtype=bool)
        localmax[:,1:-1] = (tbsum[:,1:-1] > tbsum[:,0:-2])
        localmax[:,1:-1] &= (tbsum[:,1:-1] > tbsum[:,2:])
        
        tbmax = np.copy(tbsum)
        tbmax[np.logical_not(localmax)]=0
        
        
        #foo, bar = np.histogram(tbmax.flatten(), bins)
        foo, bar = np.histogram(tbsum.flatten(), bins)
        inner_hist += foo
    return inner_hist

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        files = glob.glob(DEFAULT_DATA_FOLDER + '/*')
    else:
        files = sys.argv[1:]
    hist, bin_edges = np.histogram((), bins)
    for f in files:
        hist += sum_events(f)
    index = hist > 0
    width = bins[1] - bins[0]
    plt.bar(bins[:-1], hist, width, align='edge', log=True)
    right = np.amax(bins[:-1]*index) + 2*width
    plt.xlim(-20, right)
    plt.xlabel('Time-summed ADC Value')
    plt.ylabel('Count')
    out_filename = ('tbsum_' + files[0][len(DEFAULT_DATA_FOLDER):]
            + '.png')
    plt.show()

