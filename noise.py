#!/usr/bin/env python3

#import defaults
import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import itertools
import argparse
from roi import regions_of_interest



if __name__ == "__main__":

    # ------------------------------------------------------------------------
    # generate a parser for the command line arguments
    parser = argparse.ArgumentParser(description='Generate a pulse-height plot.')
    parser.add_argument('filename', help='the TRD raw data file to process')
    parser.add_argument('--printargs', action='store_true',
                        help='print arguments and exit')

    args = parser.parse_args()

    if args.printargs:
        print (args)
        exit(0)


    # ------------------------------------------------------------------------
    # Load the data from the preprocessed .npy file
    alldata = np.load(args.filename)


    # ------------------------------------------------------------------------
    # Do the analysis
    d = np.average(alldata, axis=(0,3) )

    hist, bins = np.histogram(d.flatten(), np.linspace(0, 20, 2000))
    width = bins[1] - bins[0]
    plt.bar(bins[:-1], hist, width, align='edge', log=False)
    plt.show()
